import {Observable} from "rxjs";

export interface Backend {

  // Prix par type de culture, retournés par le backend
  getPrices(): Observable<Array<{ type: string, price: number }>>;

  // Culture, par champ, retournés par le backend
  // spécifie le numéro du champ, le type de culture et la quantité pour ce champ.
  getCultures(): Observable<Array<{ field: number, type: string, qty: string }>>;

}

export class BackendStub implements Backend {

  getPrices(): Observable<Array<{ type: string; price: number }>> {
    return Observable.of([
      {type: "PDT", price: 10.10},
      {type: "MAI", price: 12.53},
      {type: "CAR", price: 11.38}
    ])
  }

  getCultures(): Observable<Array<{ field: number; type: string; qty: string }>> {
    return Observable.of([
      {
        field: 1,
        type: "PDT",
        qty: "1200"
      },
      {
        field: 2,
        type: "PDT",
        qty: "34"
      },
      {
        field: 1,
        type: "MAI",
        qty: "50"
      },
      {
        field: 1,
        type: "CAR",
        qty: "88"
      }, {
        field: 2,
        type: "CAR",
        qty: "88"
      }
    ]);
  }

}

