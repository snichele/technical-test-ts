const pckStandardWebpack3Config = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").pckStandardWebpack3Config;
const TypescriptSupport = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").TypescriptSupport;
const JsonRequireSupport = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").JsonRequireSupport;
const WebpackEntriesDefinition = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").WebpackEntriesDefinition;
const Vendor = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").Vendor;
const ProjectLayout = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").ProjectLayout;
const HtmlIndex = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").HtmlIndex;
const Application = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").Application;
const NG2Support = require("./node_modules/frontend-build-webpack/dist/frontend-build-api").NG2Support;
const CopyWebpackPlugin = require('copy-webpack-plugin');

const path = require("path");

const webpack = require("webpack");

let layout = new ProjectLayout();

let projectAbsoluteRootDir = process.cwd();
let app = new Application(
  "hello-angular", "index.ts"
).addHtmlIndexes([
  new HtmlIndex(
    "./src/main/frontend/hello-angular/index.ejs", "index.html", false
  ).withVendorsIds([
    "vendor-utils",
    "vendor-angular2"/*,
    "shared"*/
  ])
]);

let config = pckStandardWebpack3Config(
  new WebpackEntriesDefinition(
    projectAbsoluteRootDir,
    layout.frontEndSourcesRoot
  ).addApplications([
    app,
    new Application(
      "pck-front-use-cases", "index.ts"
    ).addHtmlIndexes([
      new HtmlIndex(
        "./src/main/frontend/pck-front-use-cases/index.ejs", "index.html", false
      ).withVendorsIds([
        "vendor-utils",
        "vendor-angular2",
        "pacifica-libs",
        "pacifica-libs-ng2"
      ])
    ])
  ]).addVendors(
    /*
      VENDORS ORDER IS IMPORTANT !
      If a vendor lib A require some elements provided in another vendor B, the other
      vendor B must be declared before A, otherwise, the vendor A will contains chuncks
      of B !
    */
    [
      new Vendor("shims", [
        "core-js",
        layout.frontEndSourcesRoot + 'shared/shims'
      ]),
      new Vendor("vendor-utils", [
        "reflect-metadata",
        "zone.js",
        "moment",
        "moment/min/locales",
        "rxjs"
      ]),
      new Vendor("pacifica-libs", [
        "typescriptz",
        "typescript-bean-validation",
        "pacifica-json-serialization",
        "pacifica-referentiel",
      ]),
      new Vendor("vendor-angular2", [
        "@angular/common",
        "@angular/compiler",
        "@angular/core",
        "@angular/forms",
        "@angular/http",
        "@angular/platform-browser",
        "@angular/platform-browser-dynamic",
        "@angular/router"
      ]),
      new Vendor("pacifica-libs-ng2", [
        "ng2-service-remote",
        "pacifica-ng2-ui",
        "jquery",

      ])
    ]),
  projectAbsoluteRootDir,
  layout.contextRoot,
  false,
  [
    new TypescriptSupport("./src/main/frontend/tsconfig.json", void 0),
    new JsonRequireSupport(),
    {
      enhanceConfig: (config) => {
        // For assets like images and font correct location
        // config.output.publicPath = "../../../frontend/";

        // config.plugins.push(new webpack.NamedModulesPlugin());
        /*var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
        config.plugins.push(new BundleAnalyzerPlugin());*/

        config.plugins.push(new CopyWebpackPlugin([
          {
            from: "./src/main/assets/classes.png",
            to: "./assets/classes.png"
          }
        ]));

        config.plugins.push(
          new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows

            // For Angular 5, see also https://github.com/angular/angular/issues/20357#issuecomment-343683491
            /\@angular(\\|\/)core(\\|\/)esm5/,
            path.resolve(__dirname, './src/main/frontend/'), // location of your src
            {
              // your Angular Async Route paths relative to this root directory
            }
          ),
        );

      }
    },
    new NG2Support()/*,
    new HtmlSupport()*/
  ]
  )
;

// Override source map control
config.devtool = void 0;

module.exports = config;
