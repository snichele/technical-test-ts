module.exports = {
  localDevProxy: {
    listenOnPort: '80',
    staticsPath: '.',
    snetStaticsPath: './src/main/frontend/sesamenet/', // TODO snet ?
    backendContextPath: '/newsesame-back-web/',
    backendURL : 'http://s00slr100.ca-pacifica.fr:8085/newsesame-back-web/', // DEV
    frontalContextPath: '/nsnet-frontal-web/',
    frontalURL: 'http://localhost:8080/nsnet-frontal-web/'
  }
}
